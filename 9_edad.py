print('Ingrese su edad', end=': ')
edad = int(input())

if edad < 0 or edad > 120:
    print('Tu edad no tiene sentido')
elif edad >= 18:
    print('Tienes la edad suficiente, puedes pasar')
elif edad < 18:
    print('No tienes la edad legal, para entrar aqui')