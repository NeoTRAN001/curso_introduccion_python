# 10 % 2 -> 0 | 9 % 2 -> !0

for i in range(2, 101): # 2 - 100
    if i % 2 == 0:
        print('El numero ', i, ' es par')
    else:
        print('El numero ', i, ' es impar')