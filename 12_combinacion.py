import math
from scipy.special import comb
#           n!
#nCm =  -----------
#       m! (n - m)! 

def show_format(n, m, result):
    print(f"""
                {n}!
    {n}C{m} =  ------------ = {result}
            {m}! ({n} - {m})!
    """)

def scipy_method(n, m):
    result = comb(n, m)
    show_format(n, m, result)

def math_method_one(n, m):
    result = math.factorial(n) / (math.factorial(m) * math.factorial(n-m))
    show_format(n, m , result)

def custom_method(n, m):
    n_factorial = n
    m_factorial = m
    n_m_factorial = n - m

    if n == m:
        return show_format(n, m, 1.0)

    for i in range(1, n):
        n_factorial *= i
    
    for i in range(1, m):
        m_factorial *= i

    for i in range(1, n - m):
        n_m_factorial *= i

    result = n_factorial / (m_factorial * n_m_factorial)

    show_format(n, m, result)
    
def main():
    n = int(input('Ingresar el valor de n: '))
    m = int(input('Ingresar el valor de m: '))

    if m > n:
        return print("Valor no valido")

    custom_method(n, m)
    math_method_one(n, m)
    scipy_method(n, m)
    

if __name__ == '__main__':
    main()