
# Operadores aritmeticos

print('''
    **************************
    *                        *
    * Operadores aritmeticos *
    *                        *
    **************************
''')
print ('Suma de 5 + 5: ', 5 + 5)
print ('Resta de 10 - 2', 10 - 2)
print ('La multiplicación de 5 * 5', 5 * 5)
print ('La división de 9 / 2', 9 / 2)
print ('La división de 9 // 2', 9 // 2)
print ('Modulo de 10 % 2', 10 % 2)
print ('El exponente de 6 ** 7', 6 ** 7)

print('''
    **************************
    *                        *
    * Operadores comparación *
    *                        *
    **************************
''')

print ('¿10 > 5 ? R: ', 10 > 5)
print ('¿10 > 15 ? R: ', 10 > 15)
print ('¿3 < 2 ? R: ', 3 < 2)
print ('¿3 < 30 ? R: ', 3 < 30)
print ('¿100 <= 150 ? R: ', 100 <= 150)
print ('¿100 <= 100 ? R: ', 100 <= 100)
print ('¿100 <= 99 ? R: ', 100 <= 99)
