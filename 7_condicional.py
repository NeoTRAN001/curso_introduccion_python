# Semáforo
# color: verde -> Adelante
# Color: rojo -> Detengase
# Color: amarillo -> Alerta
# Color: cualquier otro -> COlor no valido

color = "azul"

if color == "verde":
    print('Adelante')
elif color == "amarillo":
    print('Alerta')
elif color == "rojo":
    print('Detengase')
else:
    print("Color no valido")